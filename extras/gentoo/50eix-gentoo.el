(add-to-list 'load-path "@SITELISP@")
(autoload 'eix-diff "eix"
  "Show eix cache differences." t)
(autoload 'eix-local-update "eix"
  "Update eix database of local packages." t)
(autoload 'eix-remote-update "eix"
  "Update eix database of remote packages." t)
(autoload 'eix-search "eix"
  "Search for a PACKAGE." t)
(autoload 'eix-search-exact "eix"
  "Search for a package using completion of available packages." t)
(autoload 'eix-sync "eix"
  "Synchronize package repositories." t)
(autoload 'eix-test "eix"
  "Test for obsolete packages." t)
(autoload 'eix-show-updates "eix"
  "List packages that can be updated." t)
