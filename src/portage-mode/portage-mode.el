;;; portage-mode.el --- Support for Portage configuration files -*- lexical-binding: t -*-


;; This file is part of emacs-gentoo.

;; emacs-gentoo is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; emacs-gentoo is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-gentoo.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


;; Author: Maciej Barć <xgqt@riseup.net>
;; Homepage: https://gitlab.com/xgqt/emacs-gentoo
;; Version: 0.0.0
;; Package-Requires: ((emacs "24.1"))



;;; Commentary:


;; Support for Portage configuration files.
;; Major mode for Portage configuration files.

;; Initially based off of Winny's "portage.el",
;; URL: https://github.com/winny-/emacs.d/blob/master/site-lisp/portage.el



;;; Code:


(require 'conf-mode)
(require 'diff-mode)


(defvar portage-mode-hook nil
  "Hook for `portage-mode'.")

(defvar portage-mode--font-lock-keywords
  '(;; package atom
    ("^[A-Za-z0-9/_:=><.*-~]+" 0 'font-lock-function-name-face)
    ;; negative (-) useflag
    ("[ \t]+\\(-[~0-9A-Za-z_-]*\\)" 1 'diff-removed)
    ;; positive (+) useflag
    ("[ \t]+\\([0-9A-Za-z_.+-]*\\)" 1 'diff-added)
    ;; accepted keyword (~/**)
    ("[ \t]+\\(~[0-9A-Za-z_.-]+\\)" 1 'diff-added)
    ("[ \t]+\\(\\*\\*\\)" 1 'diff-added))
  "Font-lock keywords for `portage-mode'.")


;;;###autoload
(define-derived-mode portage-mode conf-unix-mode "Conf[Portage]"
  "Major mode for Portage configuration files."
  (conf-mode-initialize "#" 'portage-mode--font-lock-keywords))

;;;###autoload
(add-to-list
 'auto-mode-alist
 '("package\\.\\(accept_.*\\|use.*\\|unmask\\|mask\\|env\\)" . portage-mode))


(provide 'portage-mode)



;;; portage-mode.el ends here
