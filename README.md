# Emacs-Gentoo


## About

GNU Emacs integration with Gentoo tools.


## WARNING

This repository is semi-deprecated, `eix` and `openrc` integration packages
have been moved to Gentoo's git repositories.

New upstreams can be found at:
- https://gitweb.gentoo.org/proj/emacs-eix.git/
- https://gitweb.gentoo.org/proj/emacs-openrc.git/
