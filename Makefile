# This file is part of emacs-gentoo.

# emacs-gentoo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# emacs-gentoo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with emacs-gentoo.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


EMACS       := emacs
FIND        := find
RM          := rm
RMDIR       := $(RM) -r

EXTRASDIR   := $(PWD)/extras
SRCDIR      := $(PWD)/src

CACHEDIR    := $(PWD)/.cache
IDIR        := $(CACHEDIR)/image
BINIDR      := $(IDIR)/usr/bin
EMACSIDIR   := $(IDIR)/usr/share/emacs/site-lisp

EMACFLAGS   := --batch -q --no-site-file
EMACSCMD     = $(EMACS) $(EMACFLAGS)


all: clean compile


clean-%:
	$(FIND) $(SRCDIR)/$(*) -iname "*.elc" -delete

clean-cache:
	if [ -d $(CACHEDIR) ] ; then $(RMDIR) $(CACHEDIR) ; fi

clean: clean-cache clean-eix clean-openrc clean-portage-mode


compile-%:
	$(EMACSCMD) \
		--directory=$(SRCDIR)/$(*) \
		--eval "(byte-recompile-directory \"$(SRCDIR)/$(*)\" 0)"

compile: compile-eix compile-openrc compile-portage-mode


install-%: compile-%
	$(EMACSCMD) \
		--eval "(require 'package)" \
		--eval "(package-install-file \"$(SRCDIR)/$(*)\")"

install: install-eix install-openrc install-portage-mode


image-lib-%:
	mkdir -p $(EMACSIDIR)
	cp -r $(SRCDIR)/$(*) $(EMACSIDIR)

image-site-gentoo-%:
	mkdir -p $(EMACSIDIR)/site-gentoo.d
	cp -r $(EXTRASDIR)/gentoo/50$(*)-gentoo.el $(EMACSIDIR)/site-gentoo.d

image-%:
	$(MAKE) image-lib-$(*) image-site-gentoo-$(*)

image: image-eix image-openrc image-portage-mode
